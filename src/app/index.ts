import { ApolloServer, gql } from "apollo-server";
import "reflect-metadata";
import { createConnection } from "typeorm";
import { User } from "./user/User";
import { CreateUserHandler } from "./handler/CreateUserHandler";
import { ChangeUserNameHandler } from "./handler/ChangeUserNameHandler";
import { createContainer } from "./Container";
import { CommandBusFactory } from "./event-sourcing/CommandBusFactory";
import { GraphQLResolveInfo } from "graphql/type/definition";
import { defaultFieldResolver } from "graphql";
import { Payload } from "./event-sourcing/CommandBus";

const SERVER_PORT = 8080;

process.on("unhandledRejection", (error) => {
	// tslint:disable-next-line:no-console
	console.error("unhandledRejection", error);
});

(async () => {
	const typeDefs = gql`
		type User {
			id: String!
			name: String!
			email: String
		}

		type Query {
			helloWorld: String,
			users: [User!]!
		}

		type Mutation {

			createUser(
				id: String!
				name: String!
				email: String!
			): String!

			changeUserName(
				id: String!
				name: String!
			): String!

		}
	`;

	const connection = await createConnection();
	const entityManager = connection.manager;
	const container = createContainer("main", entityManager);
	const commandHandlerFactory = container.get<CommandBusFactory>(CommandBusFactory);
	const commandHandler = commandHandlerFactory.create({
		createUser: CreateUserHandler,
		changeUserName: ChangeUserNameHandler,
	});

	const resolvers = {
		Query: {
			helloWorld: () => "Yes!",
			users: () => connection.getRepository(User)
				.createQueryBuilder("u")
				.select("u.*")
				.getRawMany(),
		},
	};

	const server = new ApolloServer({
		typeDefs,
		resolvers,
		debug: process.env.NODE_ENV !== "production",
		fieldResolver: async (root: object, args: Payload, resolverContext: object, ast: GraphQLResolveInfo) => {
			if (ast.parentType.name === "Mutation") {
				await commandHandler.handle(ast.fieldName, args);
				return "OK";
			}

			return defaultFieldResolver(root, args, resolverContext, ast);
		},
	});
	const { url } = await server.listen({ port: SERVER_PORT });

	// tslint:disable-next-line:no-console
	console.log(`Server ready at ${url}`);
})();
