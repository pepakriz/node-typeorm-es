import { ColumnOptions } from "typeorm";
import { v4 as uuid } from "uuid";

const UUID_REGEX = /[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}/;

class UuidEnum {}

export type Uuid = UuidEnum & string;

export const createUuid = (value: string): Uuid => {
	if (!UUID_REGEX.test(value)) {
		throw new Error(`Invalid UUID format: ${value}`);
	}

	return value as Uuid;
};

export const generateUuid = () => createUuid(uuid());

export const UuidColumn: ColumnOptions = {
	type: "uuid",
};
