import { ColumnOptions } from "typeorm";

const EMAIL_REGEX = /^(([^<>()[\].,;:\s@"]+(\.[^<>()[\].,;:\s@"]+)*)|(".+"))@(([^<>()[\].,;:\s@"]+\.)+[^<>()[\].,;:\s@"]{2,})$/;

enum EmailEnum {}

export type Email = EmailEnum & string;

export const createEmail = (value: string): Email => {
	if (!EMAIL_REGEX.test(value)) {
		throw new Error(`Invalid email format: ${value}`);
	}

	return value as Email;
};

export const EmailColumn: ColumnOptions = {
	type: String,
};
