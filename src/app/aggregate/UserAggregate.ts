import { EntityManager } from "typeorm";
import { Aggregate, LoadableAggregate, SaveableAggregate } from "../event-sourcing/Aggregate";
import { createEmail, Email } from "../type/Email";
import { Uuid } from "../type/Uuid";
import { User } from "../user/User";

type Events = {
	name: "userCreated",
	payload: {
		name: string,
		email: Email,
	},
} | {
	name: "userNameChanged",
	payload: {
		name: string,
	},
};

export class UserAggregate extends Aggregate<Events> implements SaveableAggregate, LoadableAggregate {

	private user?: User;

	public static async createUser(args: { id: Uuid, name: string, email: Email }): Promise<UserAggregate> {
		const { id, ...payload } = args;
		const self = new UserAggregate(id);
		await self.recordEvent({
			name: "userCreated",
			payload,
		});

		return self;
	}

	public async changeName(payload: { name: string }): Promise<void> {
		await this.recordEvent({
			name: "userNameChanged",
			payload,
		});
	}

	public async save(entityManager: EntityManager): Promise<void> {
		await entityManager.save(User, this.getUser());
	}

	public async load(entityManager: EntityManager, version: number): Promise<void> {
		this.user = await entityManager.findOne(User, this.getId());
		this.version = version;
	}

	protected handleEvent(event: Events): void {
		if (event.name === "userCreated") {
			this.user = new User(
				this.getId(),
				event.payload.name,
				createEmail(event.payload.email),
			);
		} else if (event.name === "userNameChanged") {
			this.getUser().setName(event.payload.name);
		}
	}

	private getUser(): User {
		if (this.user === undefined) {
			throw new Error("User is not defined");
		}

		return this.user;
	}

}
