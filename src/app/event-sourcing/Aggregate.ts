import { EntityManager } from "typeorm";
import { Event, EventInterface } from "./Event";
import { Uuid } from "../type/Uuid";

export interface SaveableAggregate {

	save(entityManager: EntityManager): Promise<void>;

}

export interface LoadableAggregate {

	load(entityManager: EntityManager, version: number): Promise<void>;

}

export abstract class Aggregate<E extends EventInterface> {

	private readonly id: Uuid;
	protected version: number = 0;
	private recordedEvents: Event[] = [];

	public constructor(id: Uuid) {
		this.id = id;
	}

	public popRecordedEvents(): Event[] {
		const recordedEvents = this.recordedEvents;
		this.recordedEvents = [];

		return recordedEvents;
	}

	public async applyEvent(event: E): Promise<void> {
		await this.handleEvent(event);
	}

	public async replyEvents(events: Event[]): Promise<void> {
		events.forEach(async (eventEntity: Event) => {
			this.version = eventEntity.getVersion();
			await this.applyEvent(eventEntity.getEvent() as E);
		});
	}

	protected abstract handleEvent(event: E): void;

	protected async recordEvent(event: E): Promise<void> {
		this.version += 1;

		const eventEntity: Event = new Event(event.name, event.payload, this.getId(), this.constructor.name, this.version);
		this.recordedEvents.push(eventEntity);

		await this.applyEvent(event);
	}

	public getId(): Uuid {
		return this.id;
	}

}

export function isSaveableAggregate(aggregate: Aggregate<EventInterface> | SaveableAggregate): aggregate is SaveableAggregate {
	return (aggregate as SaveableAggregate).save !== undefined;
}

export function isLoadableAggregate(aggregate: Aggregate<EventInterface> | LoadableAggregate): aggregate is LoadableAggregate {
	return (aggregate as LoadableAggregate).load !== undefined;
}
