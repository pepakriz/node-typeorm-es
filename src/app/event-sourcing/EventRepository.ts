import { EntityManager } from "typeorm";
import { Aggregate, isLoadableAggregate } from "./Aggregate";
import { Uuid } from "../type/Uuid";
import { Event, EventInterface } from "./Event";
import { Service } from "typedi";

@Service()
export class EventRepository {

	private readonly entityManager: EntityManager;

	private addedAggregates: Array<Aggregate<EventInterface>> = [];

	constructor(entityManager: EntityManager) {
		this.entityManager = entityManager;
	}

	public addAggregate(aggregate: Aggregate<EventInterface>): void {
		this.addedAggregates.push(aggregate);
	}

	public async findAggregate<T extends Aggregate<EventInterface>>(aggregateType: { new(id: Uuid): T; }, aggregateId: Uuid): Promise<T> {
		const aggregate = new aggregateType(aggregateId);
		if (isLoadableAggregate(aggregate)) {
			const lastEvent: Event | void = await this.entityManager
				.createQueryBuilder(Event, "e")
				.andWhere("e.aggregateId = :aggregateId", { aggregateId })
				.andWhere("e.aggregateName = :aggregateName", { aggregateName: aggregateType.name })
				.orderBy("e.version", "DESC")
				.limit(1)
				.getOne();

			if (lastEvent === undefined) {
				throw new Error(`Aggregate ${aggregateType.name}:${aggregateId} not found`);
			}

			await aggregate.load(this.entityManager, lastEvent.version as number);
		} else {
			const events = await this.entityManager
				.createQueryBuilder(Event, "e")
				.andWhere("e.aggregateId = :aggregateId", { aggregateId })
				.andWhere("e.aggregateName = :aggregateName", { aggregateName: aggregateType.name })
				.orderBy("e.version")
				.getMany();

			if (events.length === 0) {
				throw new Error(`Aggregate ${aggregateType.name}:${aggregateId} not found`);
			}

			await aggregate.replyEvents(events);
		}

		this.addAggregate(aggregate);

		return aggregate as T;
	}

	public popAddedAggregates(): Array<Aggregate<EventInterface>> {
		const addedAggregates = this.addedAggregates;
		this.addedAggregates = [];

		return addedAggregates;
	}

}
