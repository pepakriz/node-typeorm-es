import { Column, Entity, PrimaryColumn } from "typeorm";
import { generateUuid, Uuid, UuidColumn } from "../type/Uuid";

export interface EventInterface {
	name: string;
	payload: object;
}

@Entity()
export class Event {

	@PrimaryColumn(UuidColumn)
	public readonly id: Uuid;

	@Column({ type: String })
	public readonly aggregateName: string;

	@Column(UuidColumn)
	public readonly aggregateId: Uuid;

	@Column()
	public readonly version: number;

	@Column({ type: "timestamp with time zone" })
	public readonly created: Date;

	@Column({ type: String })
	public readonly name: string;

	@Column({ type: "json" })
	public readonly payload: object;

	constructor(
		name: string,
		payload: object,
		aggregateId: Uuid,
		aggregateName: string,
		version: number,
	) {
		this.name = name;
		this.id = generateUuid();
		this.aggregateId = aggregateId;
		this.aggregateName = aggregateName;
		this.created = new Date();
		this.payload = payload !== undefined ? JSON.parse(JSON.stringify(payload)) : {};
		this.version = version;
	}

	public getEvent(): EventInterface {
		return {
			name: this.name,
			payload: this.payload,
		};
	}

	public getVersion(): number {
		return this.version;
	}

}
