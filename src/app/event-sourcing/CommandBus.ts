import { EntityManager } from "typeorm";
import { Aggregate, isSaveableAggregate } from "./Aggregate";
import { Event, EventInterface } from "./Event";
import { EventRepository } from "./EventRepository";
import { Handler } from "./Handler";
import { InputArgs } from "./InputArgs";
import { Container } from "typedi";
import { generateUuid } from "../type/Uuid";
import { ContainerInstance } from "typedi/ContainerInstance";
import { createContainer } from "../Container";

export interface Handlers {
	[key: string]: { new(...args: any[]): Handler };
}

export interface Payload {
	[key: string]: unknown;
}

export class CommandBus {

	private readonly entityManager: EntityManager;
	private readonly handlers: Handlers = {};

	constructor(
		entityManager: EntityManager,
		handlers: Handlers,
	) {
		this.entityManager = entityManager;
		this.handlers = handlers;
	}

	public async handle(name: string, payload: Payload): Promise<void> {
		const containerId = generateUuid();

		try {
			await this.entityManager.transaction(async (transactionEntityManager: EntityManager) => {
				const container = createContainer(containerId, transactionEntityManager);

				const inputArgs = new InputArgs(payload);
				const eventRepository = container.get<EventRepository>(EventRepository);

				const handler: Handler = this.getHandler(name, container);
				await handler.handle(inputArgs);

				const addedAggregates = eventRepository.popAddedAggregates();
				const eventLogInserts: Array<Promise<unknown>> = [];

				addedAggregates.forEach(async (aggregate: Aggregate<EventInterface>) => {
					if (isSaveableAggregate(aggregate)) {
						await aggregate.save(transactionEntityManager);
					}

					const recordedEvents = aggregate.popRecordedEvents();
					recordedEvents.forEach((event: Event) => {
						eventLogInserts.push(this.entityManager.insert(Event, event));
					});
				});

				await Promise.all(eventLogInserts);
			});

			Container.reset(containerId);
		} catch (error) {
			Container.reset(containerId);
			throw error;
		}
	}

	private getHandler(name: string, container: ContainerInstance): Handler {
		if (this.handlers[name] === undefined) {
			throw new Error(`Handler ${name} is not registered`);
		}

		return container.get<Handler>(this.handlers[name]);
	}

}
