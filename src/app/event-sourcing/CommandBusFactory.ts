import { Service } from "typedi";
import { EntityManager } from "typeorm";
import { CommandBus, Handlers } from "./CommandBus";

@Service()
export class CommandBusFactory {

	private readonly entityManager: EntityManager;

	constructor(
		entityManager: EntityManager,
	) {
		this.entityManager = entityManager;
	}

	public create(handlers: Handlers): CommandBus {
		return new CommandBus(this.entityManager, handlers);
	}

}
