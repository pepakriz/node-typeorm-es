import { InputArgs } from "./InputArgs";

export interface Handler {

	handle(args: InputArgs): void;

}
