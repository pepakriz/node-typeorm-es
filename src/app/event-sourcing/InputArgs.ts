import { createUuid, Uuid } from "../type/Uuid";
import { createEmail, Email } from "../type/Email";

interface Values {
	[key: string]: unknown;
}

export class InputArgs {

	private readonly values: Values;
	private readonly path: string[];

	constructor(values: Values, path: string[] = []) {
		this.values = values;
		this.path = path;
	}

	public getString(key: string): string {
		const value = this.getValue(key);
		if (typeof value !== "string") {
			throw new Error(`Argument path ${this.getFullPath([key])} is not string`);
		}

		return value;
	}

	public getUuid(key: string): Uuid {
		return createUuid(this.getString(key));
	}

	public getEmail(key: string): Email {
		return createEmail(this.getString(key));
	}

	private getValue(key: string): unknown {
		if (!this.hasValue(key)) {
			throw new Error(`Argument path ${this.getFullPath([key])} does not exist in arguments`);
		}

		return this.values[key];
	}

	private hasValue(key: string): boolean {
		return this.values[key] !== undefined;
	}

	private getFullPath(path: string[]): string {
		return [...this.path, ...path].join(".");
	}

}
