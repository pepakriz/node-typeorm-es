import { Column, Entity, PrimaryColumn } from "typeorm";
import { Email, EmailColumn } from "../type/Email";
import { Uuid, UuidColumn } from "../type/Uuid";

@Entity()
export class User {

	@PrimaryColumn(UuidColumn)
	// @ts-ignore: without getter
	private id: Uuid;

	@Column({ type: String })
	// @ts-ignore: without getter
	private name: string;

	@Column(EmailColumn)
	// @ts-ignore: without getter
	private email: Email;

	constructor(id: Uuid, name: string, email: Email) {
		this.id = id;
		this.name = name;
		this.email = email;
	}

	public setName(name: string): void {
		this.name = name;
	}

}
