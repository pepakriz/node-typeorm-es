import { Container, ContainerInstance } from "typedi";
import { Connection, EntityManager } from "typeorm";

export function createContainer(name: string, entityManager: EntityManager): ContainerInstance {
	const container = Container.of(name);

	container.set(Connection, entityManager.connection);
	container.set(EntityManager, entityManager);

	return container;
}
