import { Handler } from "../event-sourcing/Handler";
import { UserAggregate } from "../aggregate/UserAggregate";
import { InputArgs } from "../event-sourcing/InputArgs";
import { EventRepository } from "../event-sourcing/EventRepository";
import { Service } from "typedi";

@Service()
export class ChangeUserNameHandler implements Handler {

	private readonly eventRepository: EventRepository;

	constructor(eventRepository: EventRepository) {
		this.eventRepository = eventRepository;
	}

	public async handle(args: InputArgs): Promise<void> {
		const userAggregate = await this.eventRepository.findAggregate(UserAggregate, args.getUuid("id"));
		await userAggregate.changeName({
			name: args.getString("name"),
		});
	}

}
