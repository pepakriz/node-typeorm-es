import { Handler } from "../event-sourcing/Handler";
import { EventRepository } from "../event-sourcing/EventRepository";
import { UserAggregate } from "../aggregate/UserAggregate";
import { InputArgs } from "../event-sourcing/InputArgs";
import { Service } from "typedi";

@Service()
export class CreateUserHandler implements Handler {

	private readonly eventRepository: EventRepository;

	constructor(eventRepository: EventRepository) {
		this.eventRepository = eventRepository;
	}

	public async handle(args: InputArgs): Promise<void> {
		const userAggregate = await UserAggregate.createUser({
			id: args.getUuid("id"),
			name: args.getString("name"),
			email: args.getEmail("email"),
		});

		this.eventRepository.addAggregate(userAggregate);
	}

}
